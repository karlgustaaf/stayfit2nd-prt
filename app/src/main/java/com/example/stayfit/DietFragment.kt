package com.example.stayfit

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.stayfit.DietFragment.DietData.dietList
import com.google.firebase.firestore.FirebaseFirestore

class DietFragment : Fragment() {


    object DietData {
        var dietList: ArrayList<String> = ArrayList<String>()
    }

    lateinit var adapter: SimpleRVAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val db = FirebaseFirestore.getInstance()

        if (dietList != null) {
            dietList.clear()
        }


        db.collection("diettypes")
            .get()
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    for (document in task.result!!) {
                        Log.d("Value Data", document.id + " => " + document.data)
                        Log.d("Value Data", document.id + " => " + document.data.get("value"))

                        if (document.data.get("type").toString().equals(MainActivity.DietType.type)) {
                            dietList.add(document.data.get("value").toString())
                        }
                    }

                    adapter.notifyDataSetChanged()

                } else {
                    Log.w("Value Data", "Error getting documents.", task.exception)
                }
                //
            }
        val rv = RecyclerView(context!!)
        rv.layoutManager = LinearLayoutManager(context)
        adapter = SimpleRVAdapter(dietList)
        rv.adapter = adapter
//

        return rv
    }

    /**
     * A Simple Adapter for the RecyclerView
     */
    inner class SimpleRVAdapter(private val dataSource: List<String>) :
        RecyclerView.Adapter<SimpleViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SimpleViewHolder {
            return LayoutInflater.from(parent.context)
                .inflate(R.layout.fragment_diet_type, parent, false).let {
                    SimpleViewHolder(it)
                }
        }

        override fun onBindViewHolder(holder: SimpleViewHolder, position: Int) {
            holder.textView.text = dataSource[position]
        }

        override fun getItemCount(): Int {
            return dataSource.size
        }
    }
//
    /**
     * A Simple ViewHolder for the RecyclerView
     */
    class SimpleViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var view: View = itemView
        var textView: TextView = view.findViewById(R.id.value)

    }
}
