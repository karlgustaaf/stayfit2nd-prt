package com.example.stayfit

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity

class DietTypeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_diet_type2)
    }

    fun onMailClicked(view: View) {
        val to = MainActivity.DietType.email // to who we're sending it too
        val subject = "Diet Plan"
        var content = ""
        var index = 0;
        if (DietFragment.DietData.dietList != null && DietFragment.DietData.dietList.size > 0) {
            for (i in 0 until DietFragment.DietData.dietList.size) {
                index++
                content += ""+index+" "+DietFragment.DietData.dietList.get(i) + " " + "\n"
            }
            //
        }
        val intent = Intent(Intent.ACTION_SEND)
        val addressees = arrayOf(to)
        intent.putExtra(Intent.EXTRA_EMAIL, addressees)
        intent.putExtra(Intent.EXTRA_SUBJECT, subject)
        intent.putExtra(Intent.EXTRA_TEXT, content)
        intent.setType("message/rfc822")
        startActivity(Intent.createChooser(intent, "Send Email using: "));
    }
//
}
